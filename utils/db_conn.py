"""Main function to start Api calls for Haussoft."""
from sqlalchemy import create_engine


def get_conn(params):
    """Establish connection to a given dialect and return SqlAlchemy Connection."""
    keys = [
        "dialect",
        "username",
        "password",
        "host",
        "port",
        "db"
    ]

    if any([key not in params.keys() for key in keys]):
        raise ValueError("Please provide correct DB connection params!")

    conn = "{dialect}://{username}:{password}@{host}:{port}/{db}" \
        .format(
            dialect=params.get("dialect"),
            username=params.get("username"),
            password=params.get("password"),
            host=params.get("host"),
            port=params.get("port"),
            db=params.get("db"))

    engine = create_engine(conn)

    connection = engine.connect()

    if connection.closed:
        raise BaseException('Cannot establish connection to DbSource! Params: {}'.
                            format(params))
    return connection
