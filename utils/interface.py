import os
from . import db_conn
import pandas as pd
import subprocess


def inform(func):
    """Generic decorator for sending notifications to the admin."""
    def wrapper(*args, **kwargs):
        print("Start executing {} on {}".format(func.__name__, args[0].name if args else ''))

        func(*args, **kwargs)

        print("Finished {} on {}".format(func.__name__, args[0].name if args else ''))

    return wrapper


class Interface(object):
    def __init__(self, config, dest_conn, params):
        """Create Interface instance.

        Arguments:
        name -- simple name for the interface
        description -- full description of the module
        source -- expected a dict object. needs to be additionally validated
        impfile -- sql file that should represent a stored procedure
        tablename -- final destination table where the source data is stored
        columns -- column names in the destination table. to be validated against source
        procname -- name of the stored procedure that executes the import
        """
        self.name = config['name']
        self.description = config['description']
        self.source = config['source']

        self.dest_conn = dest_conn
        self.procfiles = [proc.strip() for proc in config['procfiles'].split(',')] \
            if config['procfiles'] else None

        self.tablename = config['tablename']
        self.columns = config['columns']
        self.procname = config['procname']

        self.sqlfile = None
        self.sqlread = None
        self.source_conn = None
        self.source_type = None
        self.csvfile = None
        self.tabname = None  # for already loaded table from previous interface
        self.df = None

        self.params = params

        self.transaction = None

        self._validate_source()

        self._validate_procfiles()

        self._validate_columns()

        print('Interface `{}` successfully initalized.\n'.format(self.name))

    def _validate_source(self):
        """Make sure that the sql file exists if it is a DB source / source file exists if source is csv.

        Additionally, check if connection to source can be established if it is a DB source.
        """
        self.source_type = self.source['type']

        if not self.source_type:
            raise ValueError('Please check source type for interface: {}'.format(self.name))

        if self.source_type == 'db':
            self.sqlfile = self.source['sqlfile']

            if not os.path.isfile(self.sqlfile):
                raise IOError('SqlFile is not existing! File: {}'.format(self.sqlfile))

            with open(self.sqlfile, 'rt') as f:
                self.sqlread = f.read()

            self.source_conn = db_conn.get_conn(self.source)

        elif self.source_type == 'csv' or self.source_type == 'excel':
            self.csvfile = self.source['csvfile']

            if not os.path.isfile(self.csvfile):
                raise IOError('CSV File is not existing! File: {}'.format(self.csvfile))

        elif self.source_type == 'table':
            self.tabname = self.source['tabname']

        elif self.source_type == 'none':
            pass

        else:
            raise ValueError('Wrong source type! {}'.format(self.source_type))

    def _validate_procfiles(self):
        """Make sure that the file containing the stored procedure exists."""
        if not self.procfiles:
            return

        for procfile in self.procfiles:
            if procfile and not os.path.isfile(procfile):
                raise IOError('Procedure file is not existing! File: {}'.format(procfile))

            username = self.params.get("username")
            password = self.params.get("password")
            host = self.params.get("host")
            port = self.params.get("port")
            db = self.params.get("db")

            command = 'PGPASSWORD={} psql -h {} -d {} -p {} -U {} -f {}'.format(
                password, host, db, port, username, procfile)

            subprocess.call(command, shell=True, stdout=subprocess.DEVNULL)

            # print('File {} succesfully executed!'.format(procfile))

    def _validate_columns(self):
        """Validate source against the destination table by name and number of columns.

        Make sure that the destination table columns, stored in self.columns are the same
        in number and names as the source columns.
        """
        return True

    @inform
    def read_data(self):
        if self.source_type == 'db':
            self.df = pd.read_sql(self.sqlread, self.source_conn)

        elif self.source_type == 'csv':
            self.df = pd.read_csv(self.csvfile, dtype=str)

        elif self.source_type == 'excel':
            self.df = pd.read_excel(self.csvfile, dtype=str)

        elif self.source_type == 'table':
            # TODO: check if the table name is really existing
            return

        elif self.source_type == 'none':
            return

        else:
            raise ValueError('Wrong source type! {}'.format(self.source_type))

    @inform
    def upload_data(self):
        if self.source_type in ('table', 'none'):
            return

        self.df.to_sql(
            self.tablename,
            self.dest_conn,
            if_exists='replace',
            index=True,
            chunksize=20000)

    @inform
    def insert_data(self):

        if not self.procname:
            return

        self.transaction = self.dest_conn.begin()

        result = self.dest_conn.execute('select * from {}()'.format(self.procname))

        for row in result:
            if self.df is not None:
                print('Imported {} rows out of {}.'.format(row[0], len(self.df)))

    def commit(self):
        if self.transaction:
            self.transaction.commit()

    def rollback(self):
        if self.transaction:
            self.transaction.rollback()
