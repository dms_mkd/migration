

CREATE OR REPLACE FUNCTION imp_products_map()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN

  INSERT INTO product_map(new_name, old_name)
  SELECT i.new_name, i.old_name
  FROM imp_products_map i;

	return 1;

END;

$FUNCTION$;
