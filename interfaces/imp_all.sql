
INSERT INTO "city"("name") 

SELECT city_name FROM imp_city;

INSERT INTO address(model_name, rec_id, street, STREET_NO, zip_code, city_id)
SELECT 'contact' AS model_name, cast(partner_id AS int) AS rec_id, street, "number", zip_code, c.id
FROM IMP_ADDRESS a
JOIN city c ON c.name = a.city_name
WHERE partner_id IS NOT NULL AND street IS NOT NULL;


INSERT INTO contact(name, "ref", customer, supplier, company, address_id)
SELECT i.name, i."ref", i.customer, i.supplier, i.company, min(a.id)
FROM ADDRESS a
JOIN IMP_CONTACT i ON CAST(i.partner_id AS int) = a.rec_id
GROUP BY 1,2,3,4,5,partner_id
