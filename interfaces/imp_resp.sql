

CREATE OR REPLACE FUNCTION imp_resp()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN

	truncate table resp;
	truncate table resp_gfad;

	INSERT INTO resp(name, building_id, role_name, role_gfad, created_on, updated_on)
	SELECT i.contact, b.id, i.role_name, i.role_gfad, current_timestamp, current_timestamp
	FROM imp_resp i
	JOIN BUILDING b ON b.name = i.building;

	insert into resp_gfad(name, haus, infouid, role_gfad, created_on, updated_on, building_id)
	select value, haus, infouid, info, current_timestamp, current_timestamp, b.id
	from imp_resp_gfad i
	join building b on b.origin = i.haus;

	INSERT INTO system_sync(CREATed_on, updated_on)
	values(current_timestamp, current_timestamp);

return 1;
END;

$FUNCTION$;
