select 
(rtrim(haus) + '_' + rtrim(wtyp) + '_' + rtrim(wohn)) as contract_name,
vert as name,
wert as amount,
GueltigVon as start_date,
GueltigBis as end_date
from tVerteiler
where wohn != '' and wtyp not in ('H', 'X', 'A', '+', 'Y', 'Z', '1', 'E')
and (haus like 'A%' or haus like 'B%' or haus like 'W%' or haus like '[0-9]%')
