

CREATE OR REPLACE FUNCTION imp_gfad_vert()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN

INSERT INTO VERTEILER(contract_id, name, AMOUNT)
SELECT c.id, i.name, i.amount::numeric(10,2)
FROM IMP_GFAD_VERT i JOIN RENT_CONTRACT c ON c.name = i.contract_name
WHERE i.amount IS NOT null;

return 1;
END;

$FUNCTION$;


