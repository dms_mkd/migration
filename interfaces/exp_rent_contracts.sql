select 
    e.name, 
    e.origin,
    i.start_date,
    i.end_date,
    e.state,
    p.name as unit_name,
    tenants.names,
    a.address_id
from rent_entity e
join rent_contract c on c.entity_id = e.id
join res_interval i on i.id = e.interval_id
join (select entity_id, min(property_id) as unit_id from rent_line l group by 1) t on t.entity_id = e.id
join res_property p on p.id = t.unit_id

left join (
    select r.res_id as contract_id, min(rpa.address_id) address_id
    from res_responsibility r
    join res_partner p on p.id = r.partner_id
    join res_role rol on rol.id = r.role_id
    join res_partner_address rpa on rpa.partner_id = p.id
    where r.res_model = 'rent.contract' and rol.name = 'Tenant' and rpa.type_id = (select id from ir_selection where name = 'Billing')
    group by 1
) a on a.contract_id = c.id

left join (
    select res_id as contract_id, string_agg(p.name, ', ') as names
    from res_responsibility r
    join res_partner p on p.id = r.partner_id
    join res_role rol on rol.id = r.role_id
    where r.res_model = 'rent.contract' and rol.name = 'Tenant'
    group by 1
) tenants on tenants.contract_id = c.id;

