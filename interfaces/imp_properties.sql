CREATE OR REPLACE FUNCTION imp_properties()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN


    INSERT INTO address(model_name, rec_id, street, STREET_NO, zip_code, city_id)
    SELECT 'building' AS model_name, cast(a.address_id AS int) AS rec_id, a.street, a."number", a.zip_code, c.id
    FROM IMP_BUILDING i
    join imp_address a on a.address_id = i.address_id
    JOIN city c ON c.name = a.city_name
    WHERE i.property_type = 'res.building' and not exists (select 1 from building b where b.name = i.name);


    -- FIX IT it inserts less rows than needed
	INSERT INTO building(name, COMPANY_ID, owner_id, origin, address_id)
    SELECT b.name, c.id AS company_id, cont.id AS owner_id, COALESCE(b.origin, 'N/A'), (a.id)

    FROM IMP_BUILDING b
    JOIN address a ON a.rec_id = cast(b.address_id AS int)
    JOIN COMPANY c ON c.name = b.company
    JOIN contact cont ON cont.name = b.company
    WHERE a.model_name = 'building' AND b.property_type = 'res.building' AND cont.COMPANY=true
    and not exists (select 1 from building where name = b.name);


	UPDATE address a
	SET rec_id = b.id
	FROM building b
	WHERE b.ADDRESS_ID = a.id and not exists (select 1 from building where name = b.name);

	INSERT INTO unit(name, ADDRESS_ID, BUILDING_ID, company_id, owner_id, origin)
	SELECT i.name, b.ADDRESS_ID, b.id, b.COMPANY_ID, b.OWNER_ID, COALESCE(i.origin, 'N/A')
	FROM IMP_BUILDING i
	JOIN building b ON b.name = i.parent_name
	WHERE i.property_type = 'res.unit' and not exists (select 1 from unit b where b.name = i.name);


    update building b
        set hausuid = i.hausuid
    from imp_hausuid i
    where i.haus = b.origin;

    update building b
        set origin = i.origin
    from imp_building i
    where i.name = b.name;

	return 1;


END;

$FUNCTION$;
