

CREATE OR REPLACE FUNCTION imp_rent_contracts()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN

	

    INSERT INTO address(model_name, rec_id, street, STREET_NO, zip_code, city_id)

    SELECT i.name AS model_name, 
    cast(a.address_id AS int) AS rec_id, a.street, a."number", a.zip_code, c.id
    FROM IMP_RENT_CONTRACTS i
    join imp_address a on a.address_id = i.address_id
    JOIN city c ON c.name = a.city_name WHERE a.street IS NOT NULL;


    INSERT INTO contact(name, REF, customer, supplier, company, ADDRESS_ID)
    SELECT i.name, i.origin, TRUE, FALSE, FALSE, a.id
    FROM IMP_RENT_CONTRACTS i 
    JOIN address a ON i.name = a.model_name


    INSERT INTO RENT_CONTRACT(
    name, origin, contact_id, company_id, owner_id, 
    BUILDING_ID, unit_id, START_DATE, end_date, status, TENANTS)
    
    SELECT
        i.name, COALESCE(i.origin, 'N/A'), c.id,
        u.COMPANY_ID, u.OWNER_ID, 
        u.BUILDING_ID, u.id, 
        COALESCE(i.start_date, '1900-01-01'), i.end_date, i.state, i.names 
    FROM imp_rent_contracts i
    JOIN contact c ON c.name = i.name
    JOIN unit u ON u.name = i.unit_name
    WHERE c.SUPPLIER = FALSE;

	return 1;

END;

$FUNCTION$;
