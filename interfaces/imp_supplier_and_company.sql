

CREATE OR REPLACE FUNCTION imp_supplier_and_company()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN
	

    INSERT INTO address(model_name, rec_id, street, STREET_NO, zip_code, city_id)
    SELECT 'contact' AS model_name, cast(ica.partner_id AS int) AS rec_id, street, "number", zip_code, c.id
    FROM imp_address a
    JOIN city c ON c.name = a.city_name
    join imp_contact_address ica on ica.address_id = a.address_id
    JOIN imp_contact i ON i.partner_id = ica.partner_id

    WHERE a.street IS NOT NULL AND ica.partner_id IS NOT NULL;

    INSERT INTO contact(name, ref, customer, supplier, company, address_id)
    SELECT b.name, b.ref, customer, supplier, company, min(a.id)
    FROM IMP_contact b
    JOIN address a ON a.rec_id = cast(b.partner_id AS int) GROUP BY 1,2,3,4,5;

    UPDATE address a
    SET rec_id = b.id
    FROM contact b
    WHERE b.ADDRESS_ID = a.id;


	INSERT INTO company (name, CONTACT_ID) SELECT name, id FROM contact WHERE company= TRUE;

	return 1;

END;

$FUNCTION$;
