select e.name contract, pt.name product_name, l.amount, l.line_type
from rent_entity e
join rent_line l on l.entity_id = e.id
join product_product p on p.id = l.product_id
join product_template pt on pt.id = p.product_tmpl_id
where line_type in ('rent', 'prepayment');