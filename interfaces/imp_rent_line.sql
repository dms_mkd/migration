

CREATE OR REPLACE FUNCTION imp_rent_line()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN
	-- FIX we have more than 1 line of same contract/product pair

INSERT INTO RENT_LINE(contract_id, PRODUCT_ID, line_type, amount)
SELECT c.id, p.id, substring(i.line_type, 1, 4), max(i.amount)
FROM imp_rent_line i
JOIN RENT_CONTRACT c ON c.name = i.contract
JOIN product p ON p.name = i.product_name
GROUP BY 1,2,3



	return 1;

END;

$FUNCTION$;
