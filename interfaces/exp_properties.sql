
select 
	pr.name, 
	pr.origin, 
	parent.name as parent_name, 
	pr.property_type, 
	pr.address_id, 
    p.name as company

from res_property pr

join res_partner p on p.id = pr.owner_id

left join res_property parent on parent.id = pr.parent_id;