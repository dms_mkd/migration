select p.name as contact, pr.name as building, pr.origin as origin,
r.name role_name, r.key role_gfad from res_role r join res_responsibility rr on rr.role_id = r.id
join res_partner p on p.id = rr.partner_id
join res_building b on b.id = rr.res_id
join res_property pr on pr.id = b.property_id
where rr.res_model = 'res.building';

