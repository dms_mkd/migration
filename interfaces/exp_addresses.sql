select a.id address_id, a.street, a."number", c.name city_name, z.name zip_code 
from address_address a
join address_zip z on z.id = a.zip_id
join address_city c on c.id = a.city_id