-- select
--     p.id as partner_id, name, "ref",
--     true as customer,
--     false as supplier,
--     false as company
-- from res_partner p
-- where contract = true
-- union all
select
    p.id as partner_id, name, "ref",
    false as customer,
    true as supplier,
    false as company
from res_partner p
where supplier = true and contract = False
union all
select
    p.id as partner_id, name, "ref",
    false as customer,
    true as supplier,
    true as company
from res_partner p
where p.id in (select partner_id from res_company);