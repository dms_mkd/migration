

CREATE OR REPLACE FUNCTION imp_products()
RETURNS INTEGER
LANGUAGE plpgsql
as $FUNCTION$

DECLARE

BEGIN

  INSERT INTO product(name, code)
  SELECT i.name, i.code
  FROM imp_products i;

	return 1;

END;

$FUNCTION$;
