import sys
import yaml
from utils import db_conn
from utils import interface

import time

from collections import OrderedDict

if len(sys.argv) > 1:
    with open(sys.argv[1], 'r') as ymlfile:
        config = yaml.load(ymlfile)

else:
    with open('migr.yml', 'r') as ymlfile:
        config = yaml.load(ymlfile)

params = config['destination']

dest_conn = db_conn.get_conn(params)

interfaces = config.get('interfaces')

ifs = dict()

for key, value in iter(interfaces.items()):
    ifs[key] = value

ifs = OrderedDict(sorted(ifs.items()))

for value in iter(ifs.values()):
    start_time = time.time()

    print('*******************************************************************\n')
    print("Starting {} at {} \n\n".format(value['name'], time.strftime("%c")))
    try:
        ifc = interface.Interface(value, dest_conn, params)

    except BaseException as e:
        print('Wrong YAML config for interface {}! Please check error: {}'
              .format(value.get('name'), e))

        print('*******************************************************************')

        continue

    try:
        ifc.read_data()

    except BaseException as e:
        print('Error on Data Read from source {}! Please check error: {}'.format(ifc.name, e))

        print('*******************************************************************')

        continue

    try:
        ifc.upload_data()

    except BaseException as e:
        print('Error on Data Upload {}! Please check error: {}'.format(ifc.name, e))

        print('*******************************************************************')

        continue

    try:
        ifc.insert_data()

        ifc.commit()

    except BaseException as e:
        print('Error on Data INSERT {}! Please check error: {}'.format(ifc.name, e))

        print('*******************************************************************')

        ifc.rollback()

        continue

    end_time = time.time()

    diff = format(end_time - start_time, '.2f')

    print('\nInterface `{}` successfully processed in {} seconds.'.format(ifc.name, diff))
    print('-----------------------------------------------------------------------------\n')
