# export PATH=$PATH:/home/dms/flask/migration;

cd /home/dms/flask/migration;
source /home/dms/flask/venv/bin/activate;
python --version;
python3 /home/dms/flask/migration/migr.py /home/dms/flask/migration/configs/resp.yaml;
